package com.clubfactory.platform.proxy.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author lianghaijun
 * @date 2018-11-19
 */
@Controller
public class RedisController {

    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping(value = "/redis", produces = "application/json")
    @ResponseBody
    public String show(@RequestParam(value = "key", defaultValue = "club-boot-framework.test.001",  required = false) String key) {
        //redisTemplate.opsForValue().set("club-boot-framework.test.001", "001");
        Object hello = redisTemplate.opsForValue().get(key);
        return String.valueOf(hello);
    }
}
