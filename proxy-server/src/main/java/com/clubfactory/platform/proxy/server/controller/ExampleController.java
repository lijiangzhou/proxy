package com.clubfactory.platform.proxy.server.controller;

import com.clubfactory.boot.autoconfigure.apollo.ClubApollo;
import com.clubfactory.platform.proxy.core.dataobject.ExampleDO;
import com.clubfactory.platform.proxy.core.service.ExampleService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by huage on 2018/8/8.
 */
@RestController
@RequestMapping("/example")
public class ExampleController {

    @Autowired
    private ExampleService exampleService;

    //使用apollo静态导入变量
    @Value("proxy.dynamic.base")
    private String baseValue;

    //支持apollo动态变更配置值
    private String dynamicValue;

    @PostConstruct
    public void init() {
        ClubApollo.bindProperty("proxy.dynamic.value", (value) -> {
            dynamicValue = value;
        });
    }

    @RequestMapping(path = "/hello", method = RequestMethod.GET)
    public String hello() {
        return "Hello World!";
    }


    @RequestMapping(path = "/dynamic", method = RequestMethod.GET)
    public String dynamic() {
        return "hello " + baseValue + " " + dynamicValue;
    }

    @RequestMapping(path = "/all", method = RequestMethod.GET)
    @ResponseBody
    public String queryAllProduct() {
        List<ExampleDO> queryResult = exampleService.queryAll();
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(queryResult);
        } catch (JsonProcessingException e) {
            return e.getMessage();
        }
    }

    @RequestMapping(path = "/{exampleId}", method = RequestMethod.GET)
    @ResponseBody
    public String queryProduct(@PathVariable Long productId) {
        ExampleDO result = exampleService.get(productId);
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(result);
        } catch (JsonProcessingException e) {
            return e.getMessage();
        }
    }
}
