package com.clubfactory.platform.proxy.server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author lianghaijun
 * @date 2018-12-04
 */
@Controller
public class HomeController {

    @RequestMapping("/")
    @ResponseBody
    public String checkHealth() {
        return "<a href=/datasource>datasource</a><br><a href=/redis>redis</a>";
    }

}
