package com.clubfactory.platform.proxy.server.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lianghaijun
 * @date 2018-11-20
 */
@Controller
@RequestMapping("/")
public class DataSourceController {

    @Autowired
    private DataSource dataSource;


    @Autowired
    @Qualifier("couponDataSource")
    private DataSource dataSource2;

    @Autowired
    @Qualifier("odoo")
    private DataSource dataSource3;

    @ResponseBody
    @RequestMapping("/router")
    public String router(@RequestParam(required = false) String tableName) throws SQLException {
        try(Connection connection = dataSource3.getConnection()) {
            String databaseProductName = connection.getMetaData().getURL();
            return databaseProductName;
        }
    }

    @ResponseBody
    @RequestMapping("/datasource")
    public String query(@RequestParam(required = false) String tableName) throws SQLException {
//        try(Connection connection = dataSource.getConnection()) {
//            String databaseProductName = connection.getMetaData().getURL();
//            return databaseProductName;
//        }
//
//        try(Connection connection = dataSource.getConnection()) {
//            List<String> tableNames = new ArrayList<>();
//            ResultSet tables = connection.getMetaData().getTables(null, null, null, null);
//            while (tables.next()) {
//                String name = tables.getString(1);
//                tableNames.add(name);
//            }
//            return StringUtils.join(tableNames, "<br>");
//        }


        try(Connection connection = dataSource2.getConnection()) {
            if(tableName == null) {
                ResultSet tables = connection.getMetaData().getTables(null, null, null, null);
                return toTable(tables);
            } else {
                ResultSet tables = connection.getMetaData().getColumns(null, null, tableName, null);
                return toTable(tables);
            }
        }
    }

    private String toTable(ResultSet result) throws SQLException {
        List<String> rows = new ArrayList<>();
        try (ResultSet tables = result) {
            List<String> columns = new ArrayList<>();
            for (int i = 0; i < tables.getMetaData().getColumnCount(); i++) {
                String column = tables.getMetaData().getColumnName(i+1);
                columns.add("<th>"+column+"</th>");
            }
            columns.add("<th> </th>");
            rows.add("<tr>"+ StringUtils.join(columns, " ")+"</tr>");

            while (tables.next()) {
                List<String> row = new ArrayList<>();
                for (int i = 0; i < tables.getMetaData().getColumnCount(); i++) {
                    String value = tables.getString(i+1);
                    row.add("<td>"+value+"</td>");
                }
                row.add("<td><a href=?tableName=" + tables.getString("TABLE_NAME") + ">column</a><td>");
                rows.add("<tr>"+StringUtils.join(row, " ")+"</tr>");

                if(tables.getString("TABLE_NAME") != null) {
                }
            }
        }
        return "<table>"+StringUtils.join(rows, " ")+"</table>";
    }
}
