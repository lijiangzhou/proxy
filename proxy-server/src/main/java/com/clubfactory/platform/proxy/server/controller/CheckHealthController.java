package com.clubfactory.platform.proxy.server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author lianghaijun
 * @date 2018-11-23
 */
@Controller
public class CheckHealthController {

    @RequestMapping("/check-health")
    @ResponseBody
    public String checkHealth() {
        return "success";
    }

}
