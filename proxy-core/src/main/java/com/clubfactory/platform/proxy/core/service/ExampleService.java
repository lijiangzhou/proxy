package com.clubfactory.platform.proxy.core.service;

import com.clubfactory.platform.proxy.core.dao.ExampleMapper;
import com.clubfactory.platform.proxy.core.dataobject.ExampleDO;
import com.clubfactory.platform.proxy.core.dataobject.ExampleStatus;
import com.clubfactory.platform.proxy.core.proxy.ExampleProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * manager为核心业务处理
 */
@Service
public class ExampleService {

    @Autowired
    private ExampleProxy exampleProxy;

    @Autowired
    private ExampleMapper exampleMapper;

    public ExampleDO get(long productId) {
        return exampleMapper.get(productId);
    }

    public List<ExampleDO> queryAll() {
        return exampleMapper.queryAll();
    }

    public long save(ExampleDO exampleDO) {
        if(exampleDO.getId() <= 0) {
            //mapper保存后, 由mapper中返回新生成的id, 然后返回
            exampleMapper.create(exampleDO);
            return exampleDO.getId();
        } else {
            exampleMapper.update(exampleDO);
            return exampleDO.getId();
        }
    }

    public void publish(ExampleDO exampleDO){
        exampleDO.setUpdateDate(new Date());
        exampleMapper.publish(exampleDO);
    }
}
