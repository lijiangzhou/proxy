package com.clubfactory.platform.proxy.core.dao;

import com.clubfactory.platform.proxy.core.dataobject.ExampleDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 *
 *  Product的CRUD
 *
 * Created by huage on 2018/8/6.
 */
@Mapper
public interface ExampleMapper {

    /**
     * 获取商品信息
     */
    public List<ExampleDO> queryAll();

    /**
     * 获取商品信息
     */
    public ExampleDO get(long productId);

    /**
     * 创建商品
     */
    public long create(ExampleDO exampleDO);

    /**
     * 创建商品
     */
    public void update(ExampleDO exampleDO);


    /**
     * 新商品上架接口, 更新上架状态
     */
    public void publish(ExampleDO exampleDO);


}
