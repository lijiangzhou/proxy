package com.clubfactory.platform.proxy.client.dto;

import com.clubfactory.platform.proxy.client.enums.ExampleDTOStatus;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by huage on 2018/8/6.
 */
@Data
public class ExampleDTO implements Serializable{

    private String id;

    private String name;

    private ExampleDTOStatus status;

}
