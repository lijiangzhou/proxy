package com.clubfactory.platform.proxy.client.service;

import com.clubfactory.boot.toolkit.result.Result;
import com.clubfactory.platform.proxy.client.dto.ExampleDTO;
import com.clubfactory.platform.proxy.client.result.ProductResult;

/**
 * Created by huage on 2018/8/9.
 */
public interface ExampleWriteService {

    /**
     * 保存商品, 返回新的商品ID
     */
    public Result<Long> save(ExampleDTO productDO);

    /**
     * 发布商品
     */
    public ProductResult publish(ExampleDTO productDO);

}
