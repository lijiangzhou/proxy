package com.clubfactory.platform.proxy.client.result;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by huage on 2018/8/23.
 */
@Data
public class PageResult<T> implements Serializable{
    /**
     * 返回消息提示
     */
    private String msg;
    /**
     * 返回消息编码
     */
    private String errorCode;

    /**
     * 具体数据对象
     */
    private T model;
    /**
     * 如果是列表,返回总条数
     */
    private int totalRecord;

    /**
     * 页码
     */
    private int pageNo;

    /**
     * 单页大小
     */
    private int pageSize;

    /**
     * 创建一个result
     */
    public PageResult() {
    }

    public PageResult(T model) {
        this.model = model;
    }


    public boolean isSuccess() {
        return this.errorCode == null;
    }

    public void value(String errorCode, String msg) {
        this.errorCode = errorCode;
        this.msg = msg;
    }
}
